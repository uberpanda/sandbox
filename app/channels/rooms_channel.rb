class RoomsChannel < ApplicationCable::Channel
  def subscribed
    stream_from "rooms_#{params['room_id']}_channel"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end

  def send_message(data)
    current_user.messages.create!(content: data['content'], room_id: data['room_id'])
    if data['content'] == '/timer' 
        TimerJob.perform_later(data['room_id'])
    end
  end
end