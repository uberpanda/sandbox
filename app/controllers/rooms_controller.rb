class RoomsController < ApplicationController
  before_action :authenticate_user!, except: :index

  def index
    redirect_to Room.first if Room.count > 0 
  end

  def show
    if !@room = Room.includes(:messages).find_by_id(params[:id]) || 
      flash[:alert] = 'Такой комнаты нет :('
      redirect_to action: :index
    end
    @messages = @room.messages.last(30);
    @rooms = Room.all
  end

  def create
    room = Room.new(room_params)
    room.user = current_user
    if !room.save
      flash[:alert] = 'Не создали :('
      redirect_to action: :index
    else
      redirect_to room
    end
  end

  def destroy
    @room = current_user.rooms.find_by_id(params[:id]) 
    if @room.nil? || !@room.delete
      flash[:alert] = 'Не удалили :('
    end
    redirect_to action: :index
  end

  private
    def room_params
      params.require(:room).permit(:name)
    end
end
