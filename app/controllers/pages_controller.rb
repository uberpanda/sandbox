class PagesController < ApplicationController
  before_action :authenticate_user!, only: :secrets
  def index; end

  def secrets; end
end
