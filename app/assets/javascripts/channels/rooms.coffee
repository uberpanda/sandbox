$ ->
  chatbox = $('#chatbox')
  if $('#chatbox').length > 0
    messages_to_bottom = -> chatbox.scrollTop(chatbox.prop("scrollHeight"))
    messages_to_bottom()
    App.chat = App.cable.subscriptions.create {
        channel: "RoomsChannel"
        room_id: chatbox.data('room-id')
      },
      connected: ->
        # Called when the subscription is ready for use on the server

      disconnected: ->
        # Called when the subscription has been terminated by the server

      received: (data) ->
        chatbox.append data['message']
        messages_to_bottom()

      send_message: (content, room_id) ->
        @perform 'send_message', content: content, room_id: room_id

    $('#new_message').submit (e) ->
      $this = $(this)
      textarea = $this.find('#message_content')
      if $.trim(textarea.val()).length > 1
        App.chat.send_message textarea.val(), chatbox.data('room-id')
        textarea.val('')
      e.preventDefault()
      return false