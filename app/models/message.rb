class Message < ApplicationRecord
    belongs_to :room, dependent: :destroy
    belongs_to :user
    validates :content, presence: true, length: {minimum: 2, maximum: 1000}

    after_create_commit { MessageBroadcastJob.perform_later(self) }
end
