class TimerJob < ApplicationJob
  queue_as :default

  def perform(room_id)
    broadcast(room_id, "Я воркер и умею считать до 0<br>")
    10.downto(0) do |i|
        sleep 1
        broadcast(room_id, "Гляди: #{i}!<br>")
    end
    sleep 1
    broadcast(room_id, "Кончил!<br>")
  end

  private

  def broadcast(room_id, message)
    ActionCable.server.broadcast "rooms_#{room_id}_channel", message: message
  end
  
end